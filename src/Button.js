import react from "react";
 
const  Button = ({children, className,id,onClick,name}) => {

    return(
     <>
       <button id={id} 
             onClick={onClick} 
             name={name} 
             className={className}> {children} </button>
     </>
    );

}

export default Button;