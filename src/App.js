// import React, { useState } from 'react';
// import './app.css';

// const App = () => {
//     const [result, setResult] = useState("");

//              const handleClick = (e) => {
//                setResult(result.concat(e.target.name));
//              };

//              const clear = () => {
//                setResult("");
//              };

//              const backspace = () => {
//                setResult(result.slice(0, -1));
//              };

//              const calculate = () => {
//                try {
//                  setResult(eval(result).toString());
//                } catch (err) {
//                  setResult("Error");
//                }
//              };
//     return (
//         <>
        //   <div className="container">
        //       <form>
        //           <input type="text" value={result} />
        //       </form>

        //       <div className="keypad">
        //           <button  onClick={clear} id="clear">AC</button>
        //           <button  onClick={backspace} id="backspace">Del</button>
        //           <button className="highlight" name="/" onClick={handleClick}>&divide;</button>
        //           <button name="7" onClick={handleClick}>7</button>
        //           <button name="8" onClick={handleClick}>8</button>
        //           <button name="9" onClick={handleClick}>9</button>
        //           <button className="highlight" name="*" onClick={handleClick}>&times;</button>
        //           <button name="4" onClick={handleClick}>4</button>
        //           <button name="5" onClick={handleClick}>5</button>
        //           <button name="6" onClick={handleClick}>6</button>
        //           <button className="highlight" name="-" onClick={handleClick}>&ndash;</button>
        //           <button name="1" onClick={handleClick}>1</button>
        //           <button name="2" onClick={handleClick}>2</button>
        //           <button name="3" onClick={handleClick}>3</button>
        //           <button className="highlight" name="+" onClick={handleClick}>+</button>
        //           <button name="0" onClick={handleClick}>0</button>
        //           <button name="." onClick={handleClick}>.</button>
        //           <button  onClick={calculate} id="result">=</button>
        //       </div>
        //   </div>
//         </>
//     );
// }

// export default App;

import "./app.css";
import { useState, useEffect } from "react";
import Button from "./Button";


function App() {
  const [prevValue, setPrevValue] = useState("");
  const [currentValue, setCurrentValue] = useState("");
  const [input, setInput] = useState("0");
  const [operator, setOperator] = useState(null);
  const [total, setTotal] = useState(false);

  const enterValue = (e) => {
    if (currentValue.includes(".") && e.target.innerText === ".") return;

    if (total) {
      setPrevValue("");
    }

    currentValue
      ? setCurrentValue((pre) => pre + e.target.innerText)
      : setCurrentValue(e.target.innerText);
    setTotal(false);
  };

  useEffect(() => {
    setInput(currentValue);
  }, [currentValue]);

  useEffect(() => {
    setInput("0");
  }, []);

  const mathOperator = (e) => {
    setTotal(false);
    setOperator(e.target.innerText);
    if (currentValue === "") return;
    if (prevValue !== "") {
      calculate();
    } else {
      setPrevValue(currentValue);
      setCurrentValue("");
    }
  };

 

  const calculate = (e) => {
    if (e.target.innerText === "=") {
      setTotal(true);
    }
    let result;
    switch (operator) {
      case "/":
        result = String(parseFloat(prevValue) / parseFloat(currentValue));
        break;

      case "+":
        result = String(parseFloat(prevValue) + parseFloat(currentValue));
        break;
      case "x":
        result = String(parseFloat(prevValue) * parseFloat(currentValue));
        break;
      case "-":
        result = String(parseFloat(prevValue) - parseFloat(currentValue));
        break;
      default:
        return;
    }
    setInput("");
    setPrevValue(result);
    setCurrentValue("");
  };

  const backspace = () => {
    setCurrentValue(currentValue.slice(0, -1));
    
   };

  const clear = () => {
    setPrevValue("");
    setCurrentValue("");
    setInput("0");
  };
  
  return (

    

    <div className="container">
    <div >  
       {input !== "" || input === "0" ? (
            <input readOnly
              value={input}
              type={"text"}
              
            />
          ) : (
            <input readOnly
              value={prevValue}
              type={"text"}
             
            />
          )}
    </div>

    <div className="keypad">
        <Button  onClick={clear} id="clear">AC</Button>
        <Button  onClick={backspace} id="backspace">Del</Button>
        <Button className="highlight"  onClick={mathOperator}>/</Button>
       
         
       {  [7,8,9].map(  (data) =>  <Button name={data} onClick={enterValue}>{data}</Button>)}

        <Button className="highlight"  onClick={mathOperator}>x</Button>
       { [4,5,6].map((data)=><Button name={data} onClick={enterValue}>{data}</Button>)}
      
        <Button className="highlight"  onClick={mathOperator}>-</Button>
       { [1,2,3].map((data)=><Button name={data} onClick={enterValue}>{data}</Button>)}
       
        <Button className="highlight"  onClick={mathOperator}>+</Button>
        <Button name="0" onClick={enterValue}>0</Button>
        <Button name="." onClick={enterValue}>.</Button>
        <Button  onClick={calculate} id="result">=</Button>
        
    </div>
</div>
  
  );
}

export default App;